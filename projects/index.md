---
layout: page
group: main
title: "Projects"
---

These are some projects I've worked on, or am currently involved in.

Each link will take you to a project page with some more information about it,
including any related posts I might have made about it.

<div class="booktab project-table">
  <table>
  {% assign projects = site.pages | where: "category", "projects" | sort: 'title' %}
    <thead>
      <tr>
        <th>Name</th><th>Description</th>
      </tr>
    </thead>
    <tbody>
      {% for project in projects %}
      <tr>
        <td><a href="{{ project.url | escape }}">{{ project.title | escape }}</a></td>
        <td>{% if project.description %}{{ project.description }}{% endif %}</td>
      </tr>
      {% endfor %}
    </tbody>
  </table>
</div>
