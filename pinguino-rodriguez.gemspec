# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "pinguino-rodriguez"
  spec.version       = "0.1.0"
  spec.authors       = ["José Joaquín Atria"]
  spec.email         = ["jjatria@gmail.com"]

  spec.summary       = %q{My personal theme, forked from minima: an ongoing experiment}
  spec.homepage      = "https://gitlab.com/jjatria/pinguino-rodriguez-theme"
  spec.license       = "MIT"

  spec.metadata["plugin_type"] = "theme"

  spec.files         = `git ls-files -z`.split("\x0").select do |f|
    f.match(%r{^(assets|_(includes|layouts|sass)/|(LICENSE|README)((\.(txt|md|markdown)|$)))}i)
  end

  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }

  spec.add_runtime_dependency "jekyll", "~> 4.3"

  spec.add_development_dependency "bundler", "~> 2.5"
  spec.add_development_dependency "rake", "~> 13.2"
end
